﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {

    private float _speed;

	void Start () {
        _speed = Random.Range(5f, 8f);
	}
	
	void Update () {
        transform.Translate(new Vector3(0, 1, 0) * _speed * Time.deltaTime);
	}
}
