﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {

    [SerializeField] private GameObject[] _enemies;
    [SerializeField] private float _delayTimer = 2f;
    private float _timer;

	void Start () {
        _timer = _delayTimer;
	}

	void Update () {
        _timer -= Time.deltaTime;
        if (_timer <= 0)
        {
            Vector3 pos = new Vector3(
                Random.Range(-1.1f, 1.4f), transform.position.y, transform.position.x);
            int enemyID = Random.Range(0, 2);
            Instantiate(_enemies[enemyID], pos, transform.rotation);
            _timer = _delayTimer;
        }
	}
}
