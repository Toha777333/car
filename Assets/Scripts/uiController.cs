﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UiController : MonoBehaviour {

    private bool _gameOver;
    private float _points;

    [SerializeField] private Text _gameOverText;
    [SerializeField] private Text _pointText;
    [SerializeField] private Button _restartBtn;
    [SerializeField] private Button _menuBtn;

    private void Start()
    {
        _points = 0;
        _gameOver = false;
    }

    void Update ()
    {
        _pointText.text = "" + (int)(_points * 10);

        if (!_gameOver)
        {
            _points += 1 * Time.deltaTime;
        }
	}

    public void GameOverTrue()
    {
        _gameOver = true;

        _gameOverText.gameObject.SetActive(true);
        _restartBtn.gameObject.SetActive(true);
        _menuBtn.gameObject.SetActive(true);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(1);
    }

    public void ExitToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
