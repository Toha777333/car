﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlauerController : MonoBehaviour {

    [SerializeField] private float _carSpeed = 5f;
    [SerializeField] private UiController _ui;
    private Rigidbody2D _rd;

	void Start () {
        _rd = GetComponent<Rigidbody2D>();
    }
	
	void Update () {
        float direction = Input.GetAxis("Horizontal");
        if (direction != 0)
        {
            Vector2 carMove = new Vector2(_carSpeed * direction, 0);
            _rd.velocity = carMove;
        }

        Vector3 pos = transform.position;
        pos.x = Mathf.Clamp(pos.x, -1.54f, 1.54f);
        transform.position = pos;
	}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            Destroy(gameObject);
            _ui.GameOverTrue();
        }
    }

    public void MoveLeft()
    {
        Vector2 carMove = new Vector2(_carSpeed * -1, 0);
        _rd.velocity = carMove;
    }

    public void MoveRight()
    {
        Vector2 carMove = new Vector2(_carSpeed, 0);
        _rd.velocity = carMove;
    }

    public void MoveStop()
    {
        _rd.velocity = Vector2.zero;
    }
}
