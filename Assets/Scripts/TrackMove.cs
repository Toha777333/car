﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackMove : MonoBehaviour {

    [SerializeField]
    private float _speed = 1f;

	void Update () {
        Vector2 offset = new Vector2(0, Time.time * _speed);
        GetComponent<Renderer>().material.mainTextureOffset = offset;
	}
}
